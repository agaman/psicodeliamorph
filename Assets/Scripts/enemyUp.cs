﻿using UnityEngine;
using System.Collections;

public class enemyUp : MonoBehaviour 
{
    public float distanciaActivacion = 10;
    public float velocidad = 0.1f;
    public float correccionAltura = -2;

    public int state; // 0 = esperando, 1 = atacando, 2 = vencido
    float distanciaEnemigo;

    Transform personaje;

    Vector2 direccion;

	// Use this for initialization
	void Start () 
    {
        personaje = GameObject.Find("Character").transform;
        state = 0;
        distanciaEnemigo = distanciaActivacion;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if(state == 0 && transform.position.x - distanciaActivacion <= personaje.position.x && transform.position.x > personaje.position.x)
        {
            state = 1;
            direccion = (new Vector2(-distanciaEnemigo, personaje.transform.position.y + correccionAltura - transform.position.y)).normalized * velocidad * Time.deltaTime;
        }
        if(state == 1) 
        {
            distanciaEnemigo += direccion.x;
            transform.position = new Vector2(personaje.transform.position.x + distanciaEnemigo, transform.position.y + direccion.y);
            if (transform.position.x < personaje.position.x) 
            {
                state = 2;
            }
        }
	}
}
