﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    Transform groundCheck;
    private bool grounded = true;

    public LayerMask foregroundMask;

    public float jumpVelocity = 10;
    public float runVelocity = 8;
	public float runnerIncrement = 2;
	public float runnerDecrement = 1.2f;
	public float maxRunnerVelocity = 20;

    [HideInInspector]
	public static int runnerType = 0; // 0=Izq, 1=Arr, 2=Der, 3=Abj
    private int lastRunnerType = -1;

    private Animator anim;

    public bool muerto = false;
    private float timeToPantallaMuerto = 1;
    private bool pantallaMuerto = false;
    public GameObject gameOver;

    public PhysicsMaterial2D mat;
    public float muerteY;

    private BoxCollider2D boxcollider;

	void Awake ()
    {
        groundCheck = transform.Find("groundCheck");
        anim = GetComponent<Animator>();
        anim.enabled = false;
        GetComponentInChildren<SpriteRenderer>().enabled = false;
        boxcollider = GetComponentInChildren<BoxCollider2D>();
	}

    void Start()
    {
        muerto = false;
        Init();
    }

    void Init()
    {
        rigidbody2D.velocity = new Vector2(runVelocity, rigidbody2D.velocity.y);
        GetComponentInChildren<SpriteRenderer>().enabled = true;
        anim.enabled = true;
        lastRunnerType = -1;
        runnerType = 0;
    }

	void Update ()
    {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }

        // Mueres si caes mucho
        if (!muerto && transform.position.y < muerteY)
        {
            muere();
        }


        if (muerto)
        {
            if (!pantallaMuerto)
            {
                timeToPantallaMuerto -= Time.deltaTime;
                if (timeToPantallaMuerto <= 0)
                {
                    pantallaMuerto = true;
                    GameObject gamOv = Instantiate(gameOver) as GameObject;
                    gamOv.SetActive(true);
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
                {
                    Destroy(gameOver);
                    // Volver a cargar el nivel
                    Application.LoadLevel(Application.loadedLevel);
                }
            }
            return;
        }

        Vector3 despl = new Vector3(boxcollider.size.x/2.0f - 0.0001f, 0.0f, 0.0f);
        bool lastGrounded = grounded;
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, foregroundMask) ||
                   Physics2D.Linecast(transform.position + despl, groundCheck.position + despl, foregroundMask) ||
                   Physics2D.Linecast(transform.position - despl, groundCheck.position - despl, foregroundMask);
        if (lastGrounded != grounded)
        {
            anim.SetBool("saltando", !grounded);
        }

        // Decrementa la velocidad periódicamente sin bajarla del mínimo cuando está alta
        float newRunVelocity = rigidbody2D.velocity.x - runnerDecrement * Time.deltaTime;
        if (newRunVelocity < runVelocity)
        {
            rigidbody2D.velocity = new Vector2(runVelocity, rigidbody2D.velocity.y);
                
        }
        else
        {
            rigidbody2D.velocity = new Vector2(newRunVelocity, rigidbody2D.velocity.y);
        }

        // Saltar
        if (Input.GetButtonDown("Jump") && grounded)
        {
            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, jumpVelocity);
        }

        // Leer las pulsaciones de tecla para cambiar los personajes
        if (Input.GetButtonDown("PersonajeIzq"))
        {
            runnerType = 0;
        }
        else if (Input.GetButtonDown("PersonajeArr"))
        {
            runnerType = 1;
        }
        else if (Input.GetButtonDown("PersonajeAbj"))
        {
            runnerType = 3;
        }
        else if (Input.GetButtonDown("PersonajeDer"))
        {
            // Si ya soy la ladrona, incrementa la velocidad
            if (runnerType == 2)
            {
                rigidbody2D.velocity += new Vector2(runnerIncrement, 0);
                if (rigidbody2D.velocity.x > maxRunnerVelocity)
                {
                    rigidbody2D.velocity = new Vector2(maxRunnerVelocity, rigidbody2D.velocity.y);
                }
            }
            else
            {
                runnerType = 2;
            }
        }

        // Cambiar de personaje
        if (lastRunnerType != runnerType)
        {
            lastRunnerType = runnerType;
            switch (lastRunnerType)
            {
                case 0:
                    anim.SetTrigger("Chef");
                    break;

                case 1:
                    anim.SetTrigger("Astronauta");
                    break;

                case 2:
                    anim.SetTrigger("Ladrona");
                    break;

                case 3:
                    anim.SetTrigger("Buzo");
                    break;

                default:
                    break;
            }
        }

    }

    void LateUpdate()
    {
        anim.SetBool("Buzo", false);
        anim.SetBool("Astronauta", false);
        anim.SetBool("Chef", false);
        anim.SetBool("Ladrona", false);
        anim.SetBool("ChefComer", false);
    }

    public void cogerObjetoDerecha()
    {
        anim.SetTrigger("ChefComer");
    }

    public void muere()
    {
        if (!muerto)
        {
            muerto = true;
            rigidbody2D.velocity = Vector2.zero;
            rigidbody2D.fixedAngle = false;

            mat.friction = 1f;
            rigidbody2D.AddTorque(800f);
            anim.enabled = false;
        }
    }
}
