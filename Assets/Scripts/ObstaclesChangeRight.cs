﻿using UnityEngine;
using System.Collections;

public class ObstaclesChangeRight : MonoBehaviour
{
    private Animator anim;

    private int lastRunnerType;

    void Start()
    {
        lastRunnerType = -1;
    }

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    // It changes the sprite depending on the character you are controlling
    void Update()
    {
        if (lastRunnerType != PlayerController.runnerType)
        {
            lastRunnerType = PlayerController.runnerType;
            switch (lastRunnerType)
            {
                case 0:
                    anim.SetTrigger("RightCroisant");
                    break;

                case 1:
                    anim.SetTrigger("RightMisil");
                    break;

                case 2:
                    anim.SetTrigger("RightCoche");
                    break;

                case 3:
                    anim.SetTrigger("RightTorpedo");
                    break;

                default:
                    break;
            }
        }
    }

    void LateUpdate()
    {
        anim.SetBool("RightTorpedo", false);
        anim.SetBool("RightMisil", false);
        anim.SetBool("RightCroisant", false);
        anim.SetBool("RightCoche", false);
    }
}
