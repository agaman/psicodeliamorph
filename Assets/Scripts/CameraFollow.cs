﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    GameObject player;
    
	void Awake ()
    {
        player = GameObject.Find("Character");
	}
	
	void Update ()
    {
        if (!player.GetComponent<PlayerController>().muerto)
        {
            transform.position = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
        }
	}
}
