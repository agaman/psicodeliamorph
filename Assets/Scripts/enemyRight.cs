﻿using UnityEngine;
using System.Collections;

public class enemyRight : MonoBehaviour
{
    public float distanciaActivacion = 10;
    public float velocidad = 0.1f;

    public int state; // 0 = esperando, 1 = atacando, 2 = vencido

    Transform personaje;

    Vector2 direccion;

    // Use this for initialization
    void Start()
    {
        personaje = GameObject.Find("Character").transform;
        state = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (state == 0 && transform.position.x - distanciaActivacion <= personaje.position.x)
        {
            state = 1;
        }
        if (state == 1)
        {
            transform.position += new Vector3(-velocidad * Time.deltaTime, 0, 0);
            if (transform.position.x < personaje.position.x - distanciaActivacion)
            {
                state = 2;
            }
        }
    }
}
