﻿using UnityEngine;
using System.Collections;

public class IntroductionController : MonoBehaviour {
    public GameObject pantallaInstrucciones;

    private bool instruccionesActivas = false;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if (instruccionesActivas)
        {
            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
            {
                Application.LoadLevel("Level1");
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
            {
                instruccionesActivas = true;
                renderer.enabled = false;
                pantallaInstrucciones.SetActive(true);
            }

            return;
        }
	}
}
