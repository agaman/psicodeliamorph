﻿using UnityEngine;
using System.Collections;

public class ObstaclesChangeLeft : MonoBehaviour
{
    private Animator anim;

    private int lastRunnerType;

    void Start()
    {
        lastRunnerType = -1;
    }

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    // It changes the sprite depending on the character you are controlling
    void Update()
    {
        if (lastRunnerType != PlayerController.runnerType)
        {
            lastRunnerType = PlayerController.runnerType;
            switch (lastRunnerType)
            {
                case 0:
                    anim.SetTrigger("IzqRata");
                    break;

                case 1:
                    anim.SetTrigger("IzqOvni");
                    break;

                case 3:
                    anim.SetTrigger("IzqTiburon");
                    break;

                default:
                    break;
            }
        }
    }

    void LateUpdate()
    {
        anim.SetBool("IzqRata", false);
        anim.SetBool("IzqOvni", false);
        anim.SetBool("IzqTiburon", false);
    }
}
