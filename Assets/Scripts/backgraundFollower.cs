﻿using UnityEngine;
using System.Collections;

public class backgraundFollower : MonoBehaviour {
	Transform personaje;

	// Use this for initialization
	void Start () {
		personaje = GameObject.Find("Character").transform;
		seguirCamara ();
	}
	
	// Update is called once per frame
	void Update () {
		seguirCamara ();
	}

	private void seguirCamara()
	{
		transform.position = new Vector3 (personaje.position.x, transform.position.y, transform.position.z);
	}
}
