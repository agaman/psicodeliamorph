﻿using UnityEngine;
using System.Collections;

public class TunelCaer : MonoBehaviour 
{
    public float vel = 5;
    public float distanciaActivacion = 10;
    public float minY = 10;
    Transform playerTrans;

	void Start () 
    { 
        playerTrans = GameObject.Find("Character").transform;
	}
	
	void Update () 
    {
        if (transform.position.y <= minY)
        {
            rigidbody2D.velocity = new Vector2(0, 0);
        }
        else if (Mathf.Abs(transform.position.x - playerTrans.position.x) <= distanciaActivacion)
        {
            rigidbody2D.velocity = new Vector2(0, -vel);
        }
	}
}
