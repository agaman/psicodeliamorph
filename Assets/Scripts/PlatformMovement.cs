﻿using UnityEngine;
using System.Collections;

public class PlatformMovement : MonoBehaviour
{
    public float velocityY;
    public float minY;
    public float maxY;
    public float distStartMove = 30;
    Transform personaje;

    float velY;

    void Awake()
    {
        personaje = GameObject.Find("Character").transform;
    }

    void Update()
    {

        switch (PlayerController.runnerType)
        {
            case 0:
                velY = 0;
                break;
            case 1:
                velY = velocityY;
                break;
            case 2:
                velY = 0;
                break;
            case 3:
                velY = -velocityY;
                break;

            default:
                break;
        }

        if (Mathf.Abs(transform.position.x - personaje.position.x) < distStartMove)
        {
            if (transform.position.y >= minY && velY < 0 || transform.position.y <= maxY && velY > 0)
            {
                rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, velY);
            }
            else
            {
                rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0);
            }
        }
        else
        {
            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0);
        }
    }
}
