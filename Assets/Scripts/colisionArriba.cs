﻿using UnityEngine;
using System.Collections;

public class colisionArriba : MonoBehaviour {
    public GameObject firePrefab;
    private Animator anim;
    private bool terminado = false;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    void OnTriggerEnter2D(Collider2D obj)
    {
        if (terminado)
        {
            return;
        }


        if (obj.tag == "Player")
        {
            terminado = true;
            if (PlayerController.runnerType == 0 || PlayerController.runnerType == 2)
            {
                // Si es el chef se come el croissant
                anim.SetTrigger("ObsArrCogerObjeto");
            }
            else
            {
                Instantiate(firePrefab, obj.transform.position, Quaternion.identity);
                Destroy(gameObject);
                GameObject.Find("Character").SendMessage("muere");
            }
        }
    }

    public void destruirObjeto()
    {
        Destroy(gameObject);
    }
}
