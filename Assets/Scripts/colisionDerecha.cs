﻿using UnityEngine;
using System.Collections;

public class colisionDerecha : MonoBehaviour {
    public GameObject firePrefab;

    void OnTriggerEnter2D(Collider2D obj)
    {
        if (obj.tag == "Player")
        {
            if (PlayerController.runnerType == 0)
            {
                // Si es el chef se come el croissant
                Destroy(gameObject);
                GameObject.Find("Character").SendMessage("cogerObjetoDerecha");
            }
            else
            {
                Instantiate(firePrefab, obj.transform.position, Quaternion.identity);
                Destroy(gameObject);
                GameObject.Find("Character").SendMessage("muere");
            }
        }
    }
}
