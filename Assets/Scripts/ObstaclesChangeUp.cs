﻿using UnityEngine;
using System.Collections;

public class ObstaclesChangeUp : MonoBehaviour
{
    private Animator anim;

    private int lastRunnerType;

    void Start()
    {
        lastRunnerType = -1;
    }

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    // It changes the sprite depending on the character you are controlling
    void Update()
    {
        if (lastRunnerType != PlayerController.runnerType)
        {
            lastRunnerType = PlayerController.runnerType;
            switch (lastRunnerType)
            {
                case 0:
                    anim.SetTrigger("ObsArrPast");
                    break;

                case 1:
                    anim.SetTrigger("ObsArrMeteor");
                    break;

                case 2:
                    anim.SetTrigger("ObsArrDin");
                    break;

                case 3:
                    anim.SetTrigger("ObsArrPiran");
                    break;

                default:
                    break;
            }
        }
    }

    void LateUpdate()
    {
        anim.SetBool("ObsArrPast", false);
        anim.SetBool("ObsArrMeteor", false);
        anim.SetBool("ObsArrDin", false);
        anim.SetBool("ObsArrPiran", false);
    }
}
