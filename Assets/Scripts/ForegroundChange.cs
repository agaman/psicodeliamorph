﻿using UnityEngine;
using System.Collections;

public class ForegroundChange : MonoBehaviour
{
    public Sprite designPersIzq;
    public Sprite designPersDer;
    public Sprite designPersArr;
    public Sprite designPersAbj;

    private int lastRunnerType;

    void Start()
    {
        lastRunnerType = -1;
    }

    // It changes the sprite depending on the character you are controlling
	void Update ()
    {
        if (lastRunnerType != PlayerController.runnerType)
        {
            lastRunnerType = PlayerController.runnerType;
            switch (lastRunnerType)
            {
                case 0:
                    transform.GetComponent<SpriteRenderer>().sprite = designPersIzq;
                    break;
                case 1:
                    transform.GetComponent<SpriteRenderer>().sprite = designPersArr;
                    break;
                case 2:
                    transform.GetComponent<SpriteRenderer>().sprite = designPersDer;
                    break;
                case 3:
                    transform.GetComponent<SpriteRenderer>().sprite = designPersAbj;
                    break;

                default:
                    break;
            }
        }
	}
}
