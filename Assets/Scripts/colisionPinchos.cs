﻿using UnityEngine;
using System.Collections;

public class colisionPinchos : MonoBehaviour {
    public GameObject firePrefab;

    void OnTriggerEnter2D(Collider2D obj)
    {
        if (obj.tag == "Player")
        {
            Instantiate(firePrefab, obj.transform.position, Quaternion.identity);
            GameObject.Find("Character").SendMessage("muere");
        }
    }
}
