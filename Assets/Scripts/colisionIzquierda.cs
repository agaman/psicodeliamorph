﻿using UnityEngine;
using System.Collections;

public class colisionIzquierda : MonoBehaviour {
    public GameObject firePrefab;

    void OnTriggerEnter2D(Collider2D obj)
    {
        if (obj.tag == "Player")
        {
            Instantiate(firePrefab, obj.transform.position, Quaternion.identity);
            Destroy(gameObject);
            GameObject.Find("Character").SendMessage("muere");
        }
    }
}
