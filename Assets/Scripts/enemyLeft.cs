﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(BoxCollider2D))]
public class enemyLeft : MonoBehaviour
{
    public float distanciaActivacion = 20;
    public float velocidad = 10;
    public int state; // 0 = esperando, 1 = atacando, 2 = ganado, 3 = vencido

    SpriteRenderer render;
    Transform personaje;

	// Use this for initialization
	void Start () 
    {
        personaje = GameObject.Find("Character").transform;
        render = GetComponentInChildren<SpriteRenderer>();
        render.enabled = false;
        //collider2D = GetComponent<BoxCollider2D>();
        collider2D.enabled = false;
        state = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (state == 0 && personaje.transform.position.x >= transform.position.x + distanciaActivacion)
        {
            state = 1;
            render.enabled = true;
            collider2D.enabled = true;
        }
        if (state == 1)
        {
            transform.position += new Vector3(velocidad*Time.deltaTime, 0, 0);
            if (transform.position.x >= personaje.transform.position.x)
            {
                state = 2;
            }
            else if (personaje.transform.position.x > transform.position.x + distanciaActivacion)
            {
                state = 3;
            }
        }
	}
}
